#!/usr/bin/env python3

import argparse
import cmd
import json
import os.path
import sys
import xmlrpc.client
import datetime
import shlex
from cmd2 import Cmd

def json_serial(obj):
	"""JSON serializer for objects not serializable by default json code"""

	if isinstance(obj, (datetime.datetime, datetime.date)):
		return obj.isoformat()
	raise TypeError ("Type %s not serializable" % type(obj))

class BasicGandiXmlRpcClient():

	ENDPOINTS = {
		1: {
			'name': 'Production',
			'url' : 'https://rpc.gandi.net/xmlrpc/',
		},
		2: {
			'name': 'Operational Test and Evaluation (OT&E)',
			'url' : 'https://rpc.ote.gandi.net/xmlrpc/',
		},
	}

	def __init__(self, config):
		self.api_methods = []
		self.config = config
		if config:
			self.init(config)

	def init(self, config):
		self.config_check(config)
		try:
			self.api = xmlrpc.client.ServerProxy(self.config['apiEndpoint'], use_datetime = True)
			self.api_methods = self.system_listmethods() 
		except BaseException as e:
			sys.stderr.write("Error while initializing XML-RPC client with endpoint: %s\n" % self.config['apiEndpoint'])
			sys.stderr.write("%s\n" % e)

	@staticmethod
	def config_create(config_filename):
		if os.path.isfile(config_filename):
			sys.stderr.write("Configuration file already exists: %s\n" % config_filename)
			sys.exit(1)

		api_endpoint_choice = 0
		while api_endpoint_choice not in __class__.ENDPOINTS.keys():
			for id, endpoint in __class__.ENDPOINTS.items():
				print("%d. %s endpoint (%s)" % (id, endpoint['name'], endpoint['url']))
			try:
				api_endpoint_choice = int(input("Please choose an endpoint: "))
			except ValueError:
				continue

		config = {
			'apiEndpoint': '%s' % __class__.ENDPOINTS[api_endpoint_choice]['url']
		}
		config['apiKey'] = input("Please enter your API key: ")

		json_config = json.dumps(config, indent=4)
		try:
			with open(config_filename, 'x') as config_file:
				config_file.write(json_config)
		except BaseException as e:
			sys.stderr.write("Error while writing configuration file '%s': %s\n" % (config_filename, e))
		print("Configuration file '%s' created:\n%s" % (config_filename, json_config))

	def config_load(self, config_filename):
		try:
			with open(config_filename) as config_file:
				config = json.load(config_file)
			self.init(config)
		except FileNotFoundError as e:
			sys.stderr.write("Exception: %s\n" % e)

	def config_save(self, config_filename):
		json_config = json.dumps(self.config, indent=4)
		try:
			with open(config_filename, 'x') as config_file:
				config_file.write(json_config)
		except BaseException as e:
			sys.stderr.write("Error while writing configuration file: %s\n" % config_filename)
			sys.stderr.write("%s\n" % e)

	def config_check(self, config):
		if not 'apiEndpoint' in config:
			raise RuntimeError("Missing configuration item: apiEndpoint")
		if not 'apiKey' in config:
			raise RuntimeError("Missing configuration item: apiKey")
	
	def config_display(self):
		print(json.dumps(self.config, indent = 4))

	def system_listmethods(self):
		try:
			return self.api.system.listMethods()
		except xmlrpc.client.Fault as f:
			sys.stderr.write("XMLRPC Fault code %d: %s\n" % (f.faultCode, f.faultString))

	def system_methodhelp(self, methodName):
		try:
			print(self.api.system.methodHelp(methodName))
		except xmlrpc.client.Fault as f:
			sys.stderr.write("XMLRPC Fault code %d: %s\n" % (f.faultCode, f.faultString))

	def system_methodsignature(self, methodName):
		try:
			print(self.api.system.methodSignature(methodName))
		except xmlrpc.client.Fault as f:
			sys.stderr.write("XMLRPC Fault code %d: %s\n" % (f.faultCode, f.faultString))

	@staticmethod
	def stringIsInt(string):
		try:
			int(string)
			return True
		except ValueError:
			return False

	def call_api(self, command, args):
		command_items = command.split('.')
		callee = self.api
		while len(command_items) > 0:
			command_item = command_items.pop(0)
			callee = getattr(callee, command_item)
		try:
			sys.stderr.write("Calling %s(%s)...\n" % ('.'.join(command.split('.')), ', '.join(args)))
			if len(args) > 0 and args[0] == 'apiKey':
				args.pop(0)
			args = [int(arg) if __class__.stringIsInt(arg) else arg for arg in args]
			args.insert(0, self.config['apiKey'])
			result = callee(*tuple(args))
			sys.stderr.write("Result:\n")
			print("%s" % json.dumps(result, default = json_serial, indent = 4))
		except xmlrpc.client.Fault as f:
			sys.stderr.write("XMLRPC Fault code %d: %s\n" % (f.faultCode, f.faultString))

class BasicGandiShell(Cmd):
	intro = 'Welcome to the Gandi.net shell.\nType help or ? to list commands or help "command" to display that specific command help.\nYou can execute API command directly (Example: domain.count).\n'
	prompt = "gandi> "

	def __init__(self, gandi_client, **kwargs):
		super().__init__(**kwargs)
		self.gandi_client = gandi_client

	def emptyline(self):
		pass

	def default(self, line):
		args = shlex.split(line.strip())
		command = args.pop(0)
		try:
			self.gandi_client.call_api(command, args)
		except AttributeError as e:
			sys.stderr.write("Unknown command: %s\n" % command)

	def morecommandnames(self):
		return self.gandi_client.api_methods

	def cmds_doc(self):
		return self.gandi_client.api_methods

	def morecommandhelp(self, cmd):
		if cmd not in self.morecommandnames():
			raise AttributeError
		self.gandi_client.system_methodhelp(cmd)

	def postcmd(self, stop, line):
		print()
		return stop

	def do_bye(self, arg):
		"""Exit Gandi.net shell.
	bye"""
		print( "\nBye!")
		return True
	
	def do_EOF(self, arg):
		"""Exit Gandi.net shell.
	Ctrl+D"""
		print("\nBye!")
		return True
	
	def do_config_create(self, arg):
		"""Create the specified configuration file.
	config_create <config_filename>"""
		self.gandi_client.config_create(arg)

	def do_config_load(self, arg):
		"""Load configuration from the specified file.
	config_load <config_filename>"""
		self.gandi_client.config_load(arg)
	
	def do_config_save(self, arg):
		"""Save configuration to the specified file.
	config_save <config_filename>"""
		self.gandi_client.config_save(arg)

	def do_config_set_api_endpoint(self, arg):
		"""Set Gandi.net XML-RPC API endpoint
	set_api_endpoint <endpoint_url>"""
		self.gandi_client.config['apiEndpoint'] = arg
		self.gandi_client.init(self.gandi_client.config)

	def do_config_set_api_key(self, arg):
		"""Set Gandi.net XML-RPC API key
	set_api_key <key>"""
		self.gandi_client.config['apiKey'] = arg

	def do_config_display(self, arg):
		"""Display configuration
	config"""
		self.gandi_client.config_display()

	def do_system_listmethods(self, arg):
		"""List API methods.
	system_listmethods"""
		for method in self.gandi_client.system_listmethods():
			print(method)

	def do_system_methodhelp(self, arg):
		"""Help for API method.
	system_methodhelp <methodName>"""
		self.gandi_client.system_methodhelp(arg)

	def do_system_methodsignature(self, arg):
		"""Signature for API method.
	system_methodsignature <methodName>"""
		self.gandi_client.system_methodsignature(arg)

class GandiCommandFile():
	def __init__(self, gandi_client, command_file):
		self.gandi_client = gandi_client
		self.command_file = command_file

	def execute(self):
		for line in self.command_file.readlines():
			args = shlex.split(line.strip())
			command = args.pop(0)
			try:
				method = getattr(self.gandi_client, command)
				method(*tuple(args))
			except AttributeError as e:
				try:
					self.gandi_client.call_api(command, args)
				except AttributeError as e:
					sys.stderr.write("Unknown command: %s\n" % command)

class DnsSecExampleGandiShell(BasicGandiShell):

	def selectedcommands(self):
		return [m for m in self.gandi_client.api_methods if m.startswith('domain.dnssec') or m.startswith('domain.zone') or m == 'domain.list']

	def morecommandnames(self):
		return self.selectedcommands()

	def cmds_doc(self):
		return self.selectedcommands()

if __name__ == '__main__':
	# Configure argument parser
	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--config-filename', help='Load the specified Gandi.net XML-RPC client configuration file')
	parser.add_argument('-C', '--create-config', action='store_true', help='Create a templated configuration file')
	parser.add_argument('-f', '--command-file', help='Loads commands from specified file line by line')
	args = parser.parse_args()

	# Create template configuration file
	if args.create_config:
		if not args.config_filename:
			sys.stderr.write("Missing mandatory argument: --config-filename\n")
			sys.exit(1)
		BasicGandiXmlRpcClient.config_create(args.config_filename)
		sys.exit(0)
	else:
		if args.config_filename:
			try:
				with open(args.config_filename) as config_file:
					config = json.load(config_file)
			except BaseException as e:
				sys.stderr.write("Error while reading configuration file: %s\n" % args.config_filename)
				sys.stderr.write("%s\n" % e)
				sys.exit(1)
		else:
			config=None
		gandi_client = BasicGandiXmlRpcClient(config)

		if args.command_file:
			if args.command_file == '-':
				GandiCommandFile(gandi_client, sys.stdin).execute()
			else:
				try:
					with open(args.command_file) as command_file:
						GandiCommandFile(gandi_client, command_file).execute()
				except FileNotFoundError as e:
					sys.stderr.write("Exception: %s\n" % e)
		else:
			# Start Gandi interpreter
			#DnsSecExampleGandiShell(gandi_client).cmdloop()
			BasicGandiShell(gandi_client).cmdloop()

