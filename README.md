# Gandi.net XML-RPC Client
This is a simple Command Line Interface XML-RPC client for the Gandi.net API that can be extended.

Type ```gandi-client.py --help``` for further information.

# Configuration file
Generate a new configuration file by running this command, then select an endpoint and specify your API key:
```bash
$ gandi-client.py --create-config --config-filename <config_filename>
```

Or you can create it and load it using the command line interface:
```
$ gandi-client.py
Welcome to the Gandi.net shell.
Type help or ? to list commands or help "command" to display that specific command help.
You can execute API command directly (Example: domain.count).

gandi> config_create my.conf.json
1. Production endpoint (https://rpc.gandi.net/xmlrpc/)
2. Operational Test and Evaluation (OT&E) endpoint (https://rpc.ote.gandi.net/xmlrpc/)
Please choose an endpoint: 2
Please enter your API key: MySecureAPIKey
Configuration file 'my.conf.json' created:
{
    "apiEndpoint": "https://rpc.ote.gandi.net/xmlrpc/",
    "apiKey": "MySecureAPIKey"
}

gandi> config_load my.conf.json
```

# Usage
## Display help for a specific command:
```
gandi> help version.info
version.info()

Return the API version number.
```

## Type a command:
```
gandi> version.info

Calling version.info()...

Result: {
    "api_version": "3.3.37"
}
```

## Execute commands from a file
```
gandi-client.py -c gandi.prod.conf.json -f gandi.example.cmd
```

# Specialization
You can specialize the class to reduce the list of exposed API calls:
```python
class DnsSecExampleGandiShell(BasicGandiShell):

    def selectedcommands(self):
        return [m for m in self.gandi_client.api_methods if m.startswith('domain.dnssec') or m.startswith('domain.zone') or m == 'domain.list']

    def morecommandnames(self):
        return self.selectedcommands()

    def cmds_doc(self):
        return self.selectedcommands()
```

